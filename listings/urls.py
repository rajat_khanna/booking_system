from django.urls import path
from listings import views


urlpatterns = [
    path("api/v1/units/", views.ListingViews.as_view())
]