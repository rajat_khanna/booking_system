from django.shortcuts import render
from listings.models import Listing, BlockedDays
from rest_framework import generics
from listings.serializers import ListingSerializer
from django.db.models import Exists, OuterRef


# /api/v1/units/?max_price=100&check_in=2021-12-09&check_out=2021-12-12
class ListingViews(generics.ListAPIView):
    queryset = Listing.objects.all()
    serializer_class = ListingSerializer
    
    def get_queryset(self):
        qs = super().get_queryset()
        if self.request.query_params.get("max_price"):
            qs = qs.filter(booking_info__price__lt=float(self.request.query_params.get("max_price")))

        if self.request.query_params.get("check_in") and self.request.query_params.get("check_out"):
            qs = qs.annotate(is_blocked=Exists(BlockedDays.objects.filter(
                booking_info__listing_id=OuterRef("id"),
                checkin_date__lte=self.request.query_params.get("check_out"), 
                checkout_date__gte=self.request.query_params.get("check_in")))).filter(
                is_blocked=False).order_by("booking_info__price")
        return qs

