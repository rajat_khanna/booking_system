from rest_framework import serializers
from listings.models import Listing, BookingInfo


class ListingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Listing
        fields = ("listing_type", "title", "country", "city", "price")